//
//  ItunesItemTableViewCell.swift
//  iTunes Mini Catalog
//
//  Created by Victor Engel on 4/27/20.
//  Copyright © 2020 Victor Engel. All rights reserved.
//

import UIKit

class ItunesItemTableViewCell: UITableViewCell
{
    @IBOutlet weak var itemThumbnailImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    
    var appStoreItem: AppStoreItem! {
        didSet {
            self.updateUI()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        self.accessoryType = selected ? .checkmark : .none
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        self.itemThumbnailImageView.isUserInteractionEnabled = true
        self.itemThumbnailImageView.addGestureRecognizer(tap)
    }
    
    // The main URL is called when the user taps the thumbnail image.
    @objc func handleTap(_ gestureRecognizer : UITapGestureRecognizer)
    {
        guard gestureRecognizer.view != nil else { return }
        
        // If we have a URL, and an app is available to handle it, go ahead and trigger it.
        guard let viewURL = URL(string: appStoreItem.viewURL) else { return }
        if UIApplication.shared.canOpenURL(viewURL) {
            UIApplication.shared.open(viewURL)
        }
    }
            
    func updateUI()
    {
        itemNameLabel.text = appStoreItem.trackName
        genreLabel.text = appStoreItem.genre
        
        let urlString = appStoreItem.artworkURL
        self.itemThumbnailImageView.image = nil
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            let networkProcessor = NetworkProcessor(request: request)
            networkProcessor.downloadData { (data, response, error) in
                //If the cell got reused, we may have a different URL by the time the result comes back. Let's check.
                if urlString == self.appStoreItem.artworkURL {
                    DispatchQueue.main.async {
                        if let imageData = data {
                            self.itemThumbnailImageView.image = UIImage(data: imageData)
                        }
                    }
                }
            }
        }
    }
}
