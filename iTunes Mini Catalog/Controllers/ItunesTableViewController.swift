//
//  ItunesTableViewController.swift
//  iTunes Mini Catalog
//
//  Created by Victor Engel on 4/27/20.
//  Copyright © 2020 Victor Engel. All rights reserved.
//

import UIKit

protocol AppStoreFetcher {
    func fetchCategoriezedAppStoreItems(withSearchTerm term: String, completion: @escaping ([String:Any]?) -> Void)
}

class ItunesTableViewController: UITableViewController, UISearchResultsUpdating {
    
    var items: [(String,[AppStoreItem])]?
    var appStoreClient : AppStoreFetcher = AppStoreClient()
    var resultSearchController = UISearchController()
    
    struct Storyboard {
        static let cellIdentifier = "ItunesItemCell"
    }
    
    // MARK: View Controller Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.searchBar.sizeToFit()
            
            tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        getDefaultAppStoreItems()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 96
    }
    
    // MARK: - Load defaults
    
    func getDefaultAppStoreItems()
    {
        let latestSearch = UserDefaults.standard.string(forKey: DefaultKeys.lastSearch) ?? ""
        if latestSearch.count < 2 { return }
        resultSearchController.searchBar.text = latestSearch
        self.appStoreClient.fetchCategoriezedAppStoreItems(withSearchTerm: latestSearch) { (items) in
            self.items = self.itemsFromJson(items)
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return items?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // #warning Incomplete implementation, return the number of rows
        if let items = items {
            let (_,itemsOfKind) = items[section]
            return itemsOfKind.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: Storyboard.cellIdentifier, for: indexPath) as! ItunesItemTableViewCell
        let (_,itemsOfKind) = (items![indexPath.section])
        
        cell.appStoreItem = itemsOfKind[indexPath.row]
        
        // Configure the cell...
        cell.selectionStyle = .none
        if (FavoritesManager.isFavorite(itemForIndexPath(indexPath))) {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        guard let items = items else {return ""}
        let (sectionHeading,_) = items[section]
        return sectionHeading
    }
    
    // MARK: - Tableview Delegate Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        FavoritesManager.favoriteItem(itemForIndexPath(indexPath))
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
    {
        FavoritesManager.unFavoriteItem(itemForIndexPath(indexPath))
    }
    
    // MARK: - Convenience method for AppStoreItems
    func itemForIndexPath(_ indexPath: IndexPath) -> AppStoreItem
    {
        //If this method gets called, we know there are items, so let's force unwrap.
        let (_,itemsOfKind) = (items![indexPath.section])
        
        return itemsOfKind[indexPath.row]
    }
    
    //MARK: - Search Controller
    func updateSearchResults(for searchController: UISearchController)
    {
        if let searchTerm = searchController.searchBar.text {
            // Perform a search only if there are at least two characters.
            if searchTerm.count >= 2 {
                UserDefaults.standard.set(searchTerm, forKey: DefaultKeys.lastSearch)
                self.appStoreClient.fetchCategoriezedAppStoreItems(withSearchTerm: searchTerm) { (items) in
                    self.items = self.itemsFromJson(items)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    //MARK: - Data structure translation
    func itemsFromJson(_ json: [String:Any]?) -> [(String,[AppStoreItem])]
    {
        guard let json = json else { return [] }
        var model = [(String,[AppStoreItem])]()
        let mediaTypes = Array(json.keys).sorted()
        for mediaType in mediaTypes {
            if let itemsOfType = json[mediaType] as? [AppStoreItem] {
                model.append((mediaType,itemsOfType))
            }
        }
        return model
    }
}
