//
//  FavoritesMaanger.swift
//  iTunes Mini Catalog
//
//  Created by Victor Engel on 4/28/20.
//  Copyright © 2020 Victor Engel. All rights reserved.
//

import Foundation

// There is probably a better place for these constants. The second item is used in the VC and is not part of the spec. (an extra), so leave here for now.
struct DefaultKeys {
    static let favorites = "Favorites"
    static let lastSearch = "LastSearch"
}

// A set works to uniquely store favorite items because of how AppStoreItem Codable is set up - assuming the uniqueID is really unique. Apple's API doesn't appear to detail that item.
var favorites : Set<AppStoreItem>?

class FavoritesManager
{
    /// Returns true if the App Store item has been selected as a favorite.
    static func isFavorite(_ item:AppStoreItem) -> Bool
    {
        if favorites == nil {
            loadSavedFavorites()
        }
        return favorites!.contains(item)
    }
    
    /// Add the argument as a favorite. Favorites are remembered across sessions.
    static func favoriteItem(_ item:AppStoreItem)
    {
        // We don't really need to check if it's already a favorite since set logic takes care of that. However, we do this to take advantage of the fact that isFavorite loads saved favorites if not already loaded.
        if !isFavorite(item) {
            favorites?.insert(item)
            saveFavorites()
        }
    }
    
    /// Removes the argument as a favorite.
    static func unFavoriteItem(_ item:AppStoreItem)
    {
        if (isFavorite(item)) {
            favorites?.remove(item)
            saveFavorites()
        }
    }
    
    /// Returns a sequence of all favorites.
    static func allFavorites() -> Set<AppStoreItem>
    {
        return favorites ?? Set<AppStoreItem>()
    }
    
    /// Populate local favorites from user defaults.
    private static func loadSavedFavorites()
    {
        if let savedFavorites = UserDefaults.standard.object(forKey: DefaultKeys.favorites) as? Data {
            let decoder = JSONDecoder()
            favorites = try? decoder.decode(Set<AppStoreItem>.self, from: savedFavorites)
        } else {
            favorites = Set<AppStoreItem>()
        }
    }
    
    /// Save favorites for persistencce across sessions.
    private static func saveFavorites()
    {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(favorites) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: DefaultKeys.favorites)
        }
    }
    
}

