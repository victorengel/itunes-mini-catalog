//
//  AppStoreItem.swift
//  iTunes Mini Catalog
//
//  Created by Victor Engel on 4/27/20.
//  Copyright © 2020 Victor Engel. All rights reserved.
//

import Foundation

//TODO: Research how to uniquely identify an item. The Apple API does not explain this. The viewURL item was a non-starter, since it seems to change from session to session. So the code was changed to use trackID. However, the API documentation does not list it as an item that is always available. Additionally, it's possible it's not really unique, i.e., the same id might be used for multiple artists. This does seem to work for the purposes of this exercise, though.
private struct APIKeys {
    static let kind       = "kind"
    static let trackID    = "trackId"           //Documentation does not say how to uniquely identify items. Let's try this.
    static let trackName  = "trackName"
    static let artworkURL = "artworkUrl60"
    static let genre      = "primaryGenreName"
    static let viewURL    = "trackViewUrl"      //Documentation lists viewURL, but JSON returned actually contains trackViewUrl
}

struct AppStoreItem : Codable, Hashable
{
    var kind           : String
    var trackName      : String
    var genre          : String
    var artworkURL     : String
    var viewURL        : String
    var uniqueID       : Int
    
    // By defining this hash, we make the set logic work. Other fields seem to change from session to session, but uniqueID apparently does not, so it serves as a good hash basis. See, above to-do item, in case it's not really a unique ID.
    func hash(into hasher: inout Hasher)
    {
        hasher.combine(uniqueID)
    }
    
    init?(dictionary: [String : Any])
    {
        guard let trackName = dictionary[APIKeys.trackName] as? String,
            let kind = dictionary[APIKeys.kind] as? String,
            let trackID = dictionary[APIKeys.trackID] as? Int,
            let artworkURL = dictionary[APIKeys.artworkURL] as? String,
            let viewURL = dictionary[APIKeys.viewURL] as? String,
            let genre = dictionary[APIKeys.genre] as? String else {
                return nil
        }
        self.kind       = kind
        self.trackName  = trackName
        self.artworkURL = artworkURL
        self.genre      = genre
        self.viewURL    = viewURL
        self.uniqueID   = viewURL.hashValue
        self.uniqueID   = trackID
    }
}
