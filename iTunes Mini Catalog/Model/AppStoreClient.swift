//
//  AppStoreClient.swift
//  iTunes Mini Catalog
//
//  Created by Victor Engel on 4/27/20.
//  Copyright © 2020 Victor Engel. All rights reserved.
//

import Foundation

var latestSearch = ""

struct AppStoreClient : AppStoreFetcher
{
    /// Returns an unsorted array of AppStoreItem matching the search term.
    private func fetchAppStoreItems(withSearchTerm term: String, completion: @escaping ([AppStoreItem]?) -> Void)
    {
        // Create an endpoint using the passed search term.
        let searchEndpoint = AppStoreEndpoint.search(term: term)
        let searchUrlRequest = searchEndpoint.request
        
        // Setup the networking to process the request.
        //TODO: The Apple API has guidance on maximum number of requests per minute. No attempt has been made to address that here. Consider adding throttling code to reduce network traffic and stay withing Apple's guidelines.
        let networkProcessor = NetworkProcessor(request:searchUrlRequest)
        networkProcessor.downloadJSON {(jsonResponse, httpResponse, error) in
            if error != nil {
                print("Error encountered: \(String(describing: error))")
                //TODO: Add error handling.
                completion(nil)
                return
            }
            DispatchQueue.main.async {
                //TODO: Apple's API has a maximum number of hits returned. Consider modifying the code to be able to handle situations where there are more hits than actually returned (paging).
                guard let json = jsonResponse,
                    let resultDictionaries = json["results"] as? [[String : Any]] else {
                        completion(nil)
                        return
                }
                
                // Convert from the returned JSON to AppStoreItem objects.
                var appStoreItems = resultDictionaries.compactMap ({ appDictionary in
                    return AppStoreItem(dictionary: appDictionary)
                })
                
                // Let's add in the favorites (don't worry about duplicates here - they are taken care of elsewhere)
                appStoreItems.append(contentsOf: FavoritesManager.allFavorites())
                
                completion(appStoreItems)
            }
        }
    }
    
    /// Returns an array of tuples. The first item is the kind of media. The second item is an array of items of that kind.
    func fetchCategoriezedAppStoreItems(withSearchTerm term: String, completion: @escaping ([String:Any]?) -> Void)
    {
        //It's possible for the results to come back out of order. To handle that, we save the query for use later to determine if it's the most recent query. If it's not the most recent query, we ignore further processing.
        latestSearch = term
        
        // Get an unordered array of app store items, and append the favorites, if any.
        fetchAppStoreItems(withSearchTerm: term) { (appStoreItems) in
            // Likely for fast typers: if the search term changed, ignore this result.
            // Not even a call to the completion handler since the request is stale.
            if latestSearch != term { return }
            if let appStoreItems = appStoreItems {
                if appStoreItems.count == 0 { return }
                
                // Let's sort the app store items by kind and then track name so we can group by kind.
                let sortedAppStoreItems = appStoreItems.sorted{ ($0.kind == $1.kind) ? $0.trackName < $1.trackName : $0.kind < $1.kind}
                
                // We want to return an array of tuples where the first item in each tuple is the kind, and the second item is an array of items of that kind, so let's initialize a variable to store them.
                var returnedItems = [String:[AppStoreItem]]()
                
                // Since we're in this loop, we know appStoreItems is not nil. Also, we return if the count is 0, so there is guaranteed to be a first item.
                // Let's loop through each kind. The variable workingKind will keep track of which one we're working on.
                var workingKind = sortedAppStoreItems.first!.kind
                var itemsOfWorkingKind = [AppStoreItem]()
                
                for sortedItem in sortedAppStoreItems {
                    let thisKind = sortedItem.kind
                    if thisKind == workingKind {
                        // We have yet another (or maybe the first) item of this kind. Just add to the accumulating array.
                        itemsOfWorkingKind.append(sortedItem)
                    } else {
                        // We found a DIFFERENT kind of media. So let's commit the array accumulated thus far and start a new one.
                        returnedItems[workingKind] = itemsOfWorkingKind
                        workingKind = thisKind
                        itemsOfWorkingKind.removeAll()
                        itemsOfWorkingKind.append(sortedItem)
                    }
                }
                // We're finished now, so commit the final kind and the associated array.
                returnedItems[workingKind] = itemsOfWorkingKind
                
                // Unlikely, but if the search term changed in this block don't call the completion handler.
                if latestSearch != term { return }
                completion(returnedItems)
            }
        }
    }
}
