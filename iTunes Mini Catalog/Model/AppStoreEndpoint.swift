//
//  AppStoreEndpoint.swift
//  iTunes Mini Catalog
//
//  Created by Victor Engel on 4/27/20.
//  Copyright © 2020 Victor Engel. All rights reserved.
//
//  The purpose of this file is to assemble the components of a URL to create the endpoint needed for the query.

import Foundation

enum AppStoreEndpoint
{
    case search(term: String)
    
    var request: URLRequest {
        var components = URLComponents(string: baseURL)!
        components.path = path
        components.queryItems = queryComponents
        
        let url = components.url!
        return URLRequest(url: url)
    }
    
    private var baseURL: String {
        return "https://itunes.apple.com/"
    }
    
    private var path: String {
        switch self {
        case .search: return "/search"
        }
    }
    
    // Current implementation provides only for the adjustment of the search term. However, updates to the app could allow the user to specify other items. Let set up the ability to do that in the future.
    private struct ParameterKeys {
        static let term = "term"
    }
    
    private struct DefaultValues {
        static let term = "nothing"
    }
    
    private var parameters: [String : Any] {
        switch self {
        case .search(let term):
            let parameters: [String : Any] = [
                ParameterKeys.term : term,
            ]
            return parameters
        }
    }
    
    private var queryComponents: [URLQueryItem] {
        var components = [URLQueryItem]()
        
        for (key, value) in parameters {
            let queryItem = URLQueryItem(name: key, value: "\(value)")
            components.append(queryItem)
        }
        return components
    }
}
