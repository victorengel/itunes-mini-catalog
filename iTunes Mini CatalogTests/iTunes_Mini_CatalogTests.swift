//
//  iTunes_Mini_CatalogTests.swift
//  iTunes Mini CatalogTests
//
//  Created by Victor Engel on 4/27/20.
//  Copyright © 2020 Victor Engel. All rights reserved.
//
//  For the purpose of this exercise, all tests are done in this object. Normally, they'd be separated

import XCTest
@testable import iTunes_Mini_Catalog

class iTunes_Mini_CatalogTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSampleAPIURL() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let expectation = XCTestExpectation(description: "Retrieve JSON from sample URL.")
        if let url = URL(string: "https://itunes.apple.com/search?term=jack+johnson") {
            URLSession.shared.dataTask(with: url) {data, reponse, error in
                if let data = data {
                    if let jsonString = String(data: data, encoding: .utf8) {
                        print(jsonString)
                        expectation.fulfill()
                    }
                }
            }.resume()
        }
        wait(for: [expectation], timeout: 10)
    }
    
    func testSampleAPIRULWithJSONSerialization() {
        let expectation = XCTestExpectation(description: "Retrieve JSON from sample URL.")
        guard let url = URL(string: "https://itunes.apple.com/search?term=jack+johnson") else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
        guard let dataResponse = data,
                  error == nil else {
                  print(error?.localizedDescription ?? "Response Error")
                  return }
            do{
                //here dataResponse received from a network request
                let jsonResponse = try JSONSerialization.jsonObject(with:
                                       dataResponse, options: [])
                print(jsonResponse) //Response result
                expectation.fulfill()
             } catch let parsingError {
                print("Error", parsingError)
           }
        }
        task.resume()
        wait(for: [expectation], timeout: 10)
    }
    
    func testAppStoreEndPointStructure() {
        let example = AppStoreEndpoint.search(term: "Jesus Christ Superstar")
        print(example.request.url!)
        let urlString = example.request.url!.absoluteString
        XCTAssertEqual(urlString, "https://itunes.apple.com/search?term=Jesus%20Christ%20Superstar")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
